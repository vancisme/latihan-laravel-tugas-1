<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@Home');
Route::get('/register', 'AuthController@Register');
Route::post('/welcome', 'AuthController@kirim');

Route::get('/data-tables', 'IndexController@table');

// CRUD Kategori
// Create
Route::get('/cast/create', 'CastController@create'); // mengarah ke form tambah data
Route::post('/cast', 'CastController@store'); // menyimpan data form ke database table cast

// Read
Route::get('/cast', 'CastController@index'); // ambil data ke data di tampilkan di blade
Route::get('/cast/{cast_id}', 'CastController@show'); // route detail cast

// Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); // route untuk mengarah ke form edit
Route::put('/cast/{cast_id}', 'CastController@update'); // route untuk mengarah ke form edit

// Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); // route delete data berdasarkan id