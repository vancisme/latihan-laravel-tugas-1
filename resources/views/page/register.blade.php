@extends('layout.master')
@section('title')
Buat Account Baru
    @endsection
    @section('content')
    
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name :</label> <br> <br>
        <input type="text" name="nama_depan"> <br> <br>
        <label>Last name :</label> <br> <br>
        <input type="text" name="nama_belakang"> <br> <br>
        <label>Gender</label> <br> <br>
        <input type="radio" name="JK">Male <br>
        <input type="radio" name="JK">Female <br><br>
        <label>Nationality</label> <br><br>
        <select name="Nationality" id=""> 
            <option value="1">Indonesia</option>
            <option value="2">Amerika</option>
            <option value="3">Inggris</option>
        </select> <br><br>
        <label>Language Spoken</label> <br><br>
        <input type="checkbox" name="Language_Spoken1">Bahasa Indonesia <br>
        <input type="checkbox" name="Language_Spoken2">English <br> 
        <input type="checkbox" name="Language_Spoken3">Other <br><br>
        <label>Bio</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
@endsection